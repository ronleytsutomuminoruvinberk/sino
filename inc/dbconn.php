<?php
$dsn  = "mysql:host=localhost;dbname=nilai_online";
$user = "root";
$pass = "admin";

try {
	$con = new PDO($dsn, $user, $pass);
	$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e){
	// Menangkap pesan kesalahan yang terjadi
	echo "Connection: ". $e->getMessage();
}

date_default_timezone_set('Asia/Jakarta');

$path_parts = pathinfo('/fix/sino/inc/');

// echo $path_parts['dirname'], "\n";
// echo $path_parts['basename'], "\n";
// echo $path_parts['extension'], "\n";
// echo $path_parts['filename'], "\n";

define("DIR_NAME", $path_parts['dirname']);