<?php
session_start();

include_once('../dbconn.php');

if (isset($_POST['submit'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $domain_id = $_POST['domain_id'];

    $sql = "SELECT `id`, `user_domain_id`, `password` FROM users WHERE `username` = ?";
    $sql_result = $con->prepare($sql);
    $sql_result->bindParam(1, $username);
    $sql_result->execute();
    $sql_result_obj = $sql_result->fetch(PDO::FETCH_OBJ);
    
    if ($sql_result_obj->user_domain_id == $domain_id && password_verify($password, $sql_result_obj->password)) {
        $_SESSION['user_id'] = $sql_result_obj->id;
        $_SESSION['domain_id'] = $sql_result_obj->user_domain_id;
        header('location: ../../pages/dashboard.php');
    } else {
        echo "Failed";
    }
}