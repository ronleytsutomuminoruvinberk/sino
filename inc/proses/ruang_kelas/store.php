<?php
session_start();

include_once('../../../inc/dbconn.php');

if (!empty($_SESSION['user_id'])) {
    $name = $_POST['name'];
    $created_by = 'ADMIN';
    $created_at = date('Y-m-d H:i:s');
    
    if (isset($_POST['submit'])) {
        $sql = "INSERT INTO class_rooms (`name`, `created_by`, `created_at`) VALUES (?, ?, ?)";
        $sql_result = $con->prepare($sql);
        $sql_result->bindParam(1, $name);
        $sql_result->bindParam(2, $created_by);
        $sql_result->bindParam(3, $created_at);
        if ($sql_result->execute()) {
            header('location: ../../../pages/class_rooms?response=success_store');
        };
    }
} else {
    header('location: ../../../login.php');
}
?>