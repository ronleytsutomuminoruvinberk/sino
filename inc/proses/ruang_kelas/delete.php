<?php
session_start();

include_once('../../../inc/dbconn.php');

if (!empty($_SESSION['user_id'])) {
    $id = $_GET['id'];

    $sql = "DELETE FROM class_rooms WHERE id = ?";
    $sql_result = $con->prepare($sql);
    $sql_result->bindParam(1, $id);
    if ($sql_result->execute()) {
        header('location: ../../../pages/class_rooms?response=success_delete');
    }
} else {
    header('location: ../../../login.php');
}
?>