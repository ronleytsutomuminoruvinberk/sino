<?php
session_start();

include_once('../../../inc/dbconn.php');

if (!empty($_SESSION['user_id'])) {
    $id = $_POST['id'];
    $name = $_POST['name'];
    $updated_by = 'ADMIN';
    $updated_at = date('Y-m-d H:i:s');

    $sql = "UPDATE `class_rooms` SET `name` = ?, `updated_by` = ?, `updated_at` = ? WHERE `id` = ?";
    $sql_result = $con->prepare($sql);
    $sql_result->bindParam(1, $name);
    $sql_result->bindParam(2, $updated_by);
    $sql_result->bindParam(3, $updated_at);
    $sql_result->bindParam(4, $id);
    if ($sql_result->execute()) {
        header('location: ../../../pages/class_rooms?response=success_update');
    }
}  else {
    header('location: ../../../login.php');
}
?>