<?php
    $response = isset($_GET['response']) ? $_GET['response'] : NULL;
    if ($response == 'success_store') {
?>
    <div class="alert alert-success" role="alert">Data has been successfully stored. Thank you.</div>
<?php
    } else {

    }

    if ($response == 'success_delete') {
?>
    <div class="alert alert-success" role="alert">Data has been successfully deleted. Thank you.</div>
<?php
    } else {

    }

    if ($response == 'success_update') {
?>
    <div class="alert alert-success" role="alert">Data has been successfully updated. Thank you.</div>
<?php
    } else {

    }
?>
<h1 class="page-header">List Class Rooms <a href="_form.php" role="button" class="btn btn-md btn-default pull-right">Create</a></h1>

<!-- start table-responsive -->
<div class="table-responsive">
    <!-- start table -->
    <table class="table table-striped" id="class_rooms_tabel">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            <tbody>
                <?php
                    $sql = "SELECT * FROM class_rooms";
                    $sql_result = $con->prepare($sql);
                    $sql_result->execute();
                    $sql_result_obj = $sql_result->fetchAll(PDO::FETCH_OBJ);
                    
                    $no = 1;
                    foreach ($sql_result_obj as $key => $value) {
                ?>
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $value->name; ?></td>
                    <td>
                        <a href="view.php?id=<?php echo $value->id; ?>" role="button" class="btn btn-md btn-info">View</a>
                        <a href="edit.php?id=<?php echo $value->id; ?>" role="button" class="btn btn-md btn-primary">Edit</a>
                        <a href="<?php echo DIR_NAME; ?>/inc/proses/ruang_kelas/delete.php?id=<?php echo $value->id; ?>" role="button" class="btn btn-md btn-danger" onclick="return confirm('Are you sure to delete this data?')">Delete</a>
                    </td>
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </thead>
    </table>
    <!-- end table -->
</div>
<!-- end table-responsive -->