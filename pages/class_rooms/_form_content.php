<?php
    echo isset($_GET['id']) ? "<h1 class='page-header'>Update data</h1>" : "<h1 class='page-header'>Add data</h1>";
    
    $id = isset($_GET['id']) ? $_GET['id'] : '';
    $sql = "SELECT `id`, `name` FROM class_rooms WHERE id = ?";
    $sql_result = $con->prepare($sql);
    $sql_result->bindParam(1, $id);
    $sql_result->execute();
    $sql_result_obj = $sql_result->fetch(PDO::FETCH_OBJ);
?>
<form action="<?php if ($id) { echo DIR_NAME . '/inc/proses/ruang_kelas/update.php?id='.$_GET['id']; } else { echo DIR_NAME . '/inc/proses/ruang_kelas/store.php'; } ?>" method="POST" class="form-horizontal">
    <input type="hidden" name="id" value="<?php echo isset($sql_result_obj->name) ? $sql_result_obj->id : '' ?>" />
    <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-4">
            <input type="text" name="name" class="form-control" id="inputName" value="<?php echo isset($sql_result_obj->name) ? $sql_result_obj->name : '' ?>" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?php if ($id) { ?> <button type="submit" name="submit" class="btn btn-success">Update</button> <?php } else { ?> <button type="submit" name="submit" class="btn btn-success">Create</button> <?php } ?>
        </div>
    </div>
</form>