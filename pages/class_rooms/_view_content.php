<?php
    $id = isset($_GET['id']) ? $_GET['id'] : '';
    $sql = "SELECT * FROM class_rooms WHERE id = ?";
    $sql_result = $con->prepare($sql);
    $sql_result->bindParam(1, $id);
    $sql_result->execute();
    $sql_result_obj = $sql_result->fetch(PDO::FETCH_OBJ);
?>
<a href="_form.php" role="button" class="btn btn-md btn-default">Create</a>
<a href="edit.php?id=<?php echo $sql_result_obj->id; ?>" role="button" class="btn btn-md btn-primary">Edit</a>
<a href="<?php echo DIR_NAME; ?>/inc/proses/ruang_kelas/delete.php?id=<?php echo $sql_result_obj->id; ?>" role="button" class="btn btn-md btn-danger" onclick="return confirm('Are you sure to delete this data?')">Delete</a>
<br /><br />
<h1 class="page-header"><?php echo isset($sql_result_obj->name) ? $sql_result_obj->name : '' ?></h1>
<h4 class="page-header">Detail <?php echo isset($sql_result_obj->name) ? $sql_result_obj->name : '' ?></h4>

<!-- start table-responsive -->
<div class="table-responsive">
    <!-- start table -->
    <table class="table table-striped">
        <tr>
            <td style="width: 15%;">Name</td>
            <td style="width: 1%">:</td>
            <td><?php echo isset($sql_result_obj->name) ? $sql_result_obj->name : '' ?></td>
        </tr>
    </table>
</div>
<!-- end table-responsive -->
<h4 class="page-header">Detail <?php echo isset($sql_result_obj->name) ? $sql_result_obj->name : '' ?></h4>

<!-- start table-responsive -->
<div class="table-responsive">
    <!-- start table -->
    <table class="table table-striped">
        <tr>
            <td style="width: 15%;">Created by</td>
            <td style="width: 1%">:</td>
            <td><?php echo isset($sql_result_obj->created_by) ? $sql_result_obj->created_by : '' ?></td>
        </tr>
        <tr>
            <td>Created at</td>
            <td>:</td>
            <td><?php echo isset($sql_result_obj->created_at) ? $sql_result_obj->created_at : '' ?></td>
        </tr>
        <tr>
            <td>Updated by</td>
            <td>:</td>
            <td><?php echo isset($sql_result_obj->updated_by) ? $sql_result_obj->updated_by : '' ?></td>
        </tr>
        <tr>
            <td>Updated at</td>
            <td>:</td>
            <td><?php echo isset($sql_result_obj->updated_at) ? $sql_result_obj->updated_at : '' ?></td>
        </tr>
    </table>
    <!-- end table -->
</div>
<!-- end table-responsive -->