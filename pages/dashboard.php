<?php
    session_start();

    if (!empty($_SESSION['user_id'])) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="canonical" href="https://getbootstrap.com/docs/3.4/examples/signin/">

    <title>SINO | Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href=".././assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href=".././assets/bootstrap/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href=".././assets/bootstrap/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src=".././assets/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <!-- start container-fluid -->
        <div class="container-fluid">
            <!-- start navbar-header -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../../pages/dashboard.php">SINO</a>
            </div>
            <!-- end navbar-header -->

            <!-- start navbar -->
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="../inc/proses/logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- end navbar -->
        </div>
        <!-- end container-fluid -->
    </nav>
    <!-- end navbar -->
    
    <!-- start container-fluid -->
    <div class="container-fluid">
        <!-- start row -->
        <div class="row">
            <!-- start col -->
            <div class="col-sm-3 col-md-2 sidebar">
            <?php
                if ($_SESSION['domain_id'] == 1) {
            ?>
            <ul class="nav nav-sidebar">
                <li><a href="./class_rooms/">Class Rooms</a></li>
                <li><a href="./lesson/">Lessons</a></li>
                <li><a href="#">Users</a></li>
                <li><a href="#">Teachings</a></li>
            </ul>
            <?php
                } elseif ($_SESSION['domain_id'] == 2) {
            ?>
            <ul class="nav nav-sidebar">
                <li><a href="#">Students</a></li>
                <li><a href="#">Export Exam</a></li>
            </ul>
            <?php
                }
            ?>
            </div>
            <!-- end col -->
            
            <!-- start col -->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header">Dashboard</h1>
                <!-- start row -->
                <div class="row placeholders">
                    <h2>Selamat datang di SINO <small>(Sistem Informasi Nilai Online)</small></h2>
                </div>
                <!-- end row -->
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container-fluid -->
</body>
</html>
<?php
    } else {
        header('location: ../login.php');
    }
?>