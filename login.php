<?php
    include_once('inc/dbconn.php');

    $sql = "SELECT `id`, `name` FROM user_domains";
    $sql_result = $con->prepare($sql);
    $sql_result->execute();
    $sql_result_array = $sql_result->fetchAll(PDO::FETCH_OBJ);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="canonical" href="https://getbootstrap.com/docs/3.4/examples/signin/">

    <title>SINO | Login Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/bootstrap/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/bootstrap/css/signin.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- start body -->

    <!-- start container -->
    <div class="container">
        <form action="inc/proses/login.php" method="POST" class="form-signin">
            <h2 class="form-signin-heading">SINO <small>Login Admin</small></h2>
            <label for="inputUsername" class="sr-only">Username</label>
            <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <select name="domain_id" class="form-control" id="">
                <option value="">--- Select data ---</option>
                <?php
                    foreach ($sql_result_array as $key => $value) {
                ?>
                    <option value="<?php echo $value->id; ?>"><?php echo ucfirst($value->name); ?></option>
                <?php
                    }
                ?>
            </select>
            <div class="form-group">
                <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">LOGIN</button>
            </div>
        </form>
        <footer style="text-align: center;">
            Created by Agus Sumarna | Modified by Ronley Vinberk | &copy; 2012 - <?php echo date('Y'); ?>
        </footer>
        <?php
            // echo dirname(__DIR__);
            // echo dirname(__FILE__);
            // $path_parts = pathinfo('sino/inc/dbconn.php');

            // echo $path_parts['dirname'], "\n";
            // echo $path_parts['basename'], "\n";
            // echo $path_parts['extension'], "\n";
            // echo $path_parts['filename'], "\n"; 
        ?>
    </div>
    <!-- end container -->

    <!-- end body -->
</body>
</html>